{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1ac0d12c",
   "metadata": {},
   "source": [
    "# Vortex Panel Method\n",
    "This notebook will use the vortex panel method to predict the lift coefficient and pressure coefficient distribution for an arbitrary airfoil shape.\n",
    "\n",
    "The user must provide:\n",
    " - The airfoil coordinates normalized by the chord length in airfoil.txt (see airfoil generators notebook)\n",
    " - The desired minimum, maximum and increments for the angle of attack (minalpha, maxalpha, alphastep)\n",
    " \n",
    "The outputs of this program include\n",
    " - Plots of the airfoil shape, lift coefficient as a function of angle of attack, and pressure coefficient as a function of chordwise position saved in the plots folder.\n",
    " - Table of the above values saved in the data folder.\n",
    " \n",
    "Once those are provided you simply need to run all the cells in the notebook to generate the output. A NACA 0012 demonstration case is included by default including reference experimental data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7294c9c9",
   "metadata": {},
   "source": [
    "Importing a few useful libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aaf13018",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3f19b76",
   "metadata": {},
   "source": [
    "Define a function to load the airfoil coordinates provided in airfoil.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d26c440f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def load_points(plot):\n",
    "    # Load the points from airfoil.txt (must be a closed trailing edge with first/last point duplicated)\n",
    "    points = np.loadtxt('airfoil.txt')\n",
    "\n",
    "    # Flip the array upside down if needed (depending on which way the nodes are ordered)\n",
    "    points = np.flipud(points)\n",
    "\n",
    "    # Get the number of panels\n",
    "    n = np.shape(points)[0] - 1\n",
    "\n",
    "    # Get the centroids of the panels\n",
    "    centroids = np.zeros((n, 2))\n",
    "    for i in range(0, n):\n",
    "        centroids[i, 0] = 0.5 * (points[i, 0] + points[i + 1, 0])\n",
    "        centroids[i, 1] = 0.5 * (points[i, 1] + points[i + 1, 1])\n",
    "\n",
    "    # Plot the airfoil shape, endpoints, and centroids\n",
    "    plot.plot(points[:, 0], points[:, 1])\n",
    "    plot.set_xlabel('x/c')\n",
    "    plot.set_ylabel('y/c')\n",
    "    plot.set_title('Airfoil')\n",
    "    plot.set_ylim([-0.5, 0.5])\n",
    "    figcp.savefig('plots/airfoil.pdf')\n",
    "\n",
    "    return n, points, centroids"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ca53cae",
   "metadata": {},
   "source": [
    "Define a function to compute the linear system coefficients for the vortex panel method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a22159c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def coefficients(points, centroids, i, j):\n",
    "    # Get the angles of both panels\n",
    "    phii = math.atan2((points[i + 1, 1] - points[i, 1]), (points[i + 1, 0] - points[i, 0]))\n",
    "    phij = math.atan2((points[j + 1, 1] - points[j, 1]), (points[j + 1, 0] - points[j, 0]))\n",
    "\n",
    "    # Get all of the terms\n",
    "    a = -(centroids[i, 0] - points[j, 0]) * math.cos(phij) - (centroids[i, 1] - points[j, 1]) * math.sin(phij)\n",
    "    b = (centroids[i, 0] - points[j, 0]) ** 2 + (centroids[i, 1] - points[j, 1]) ** 2\n",
    "    c1 = math.sin(phii - phij)\n",
    "    c2 = math.cos(phii - phij)\n",
    "    d = (centroids[i, 0] - points[j, 0]) * math.cos(phii) + (centroids[i, 1] - points[j, 1]) * math.sin(phii)\n",
    "    sj = math.sqrt((points[j + 1, 0] - points[j, 0]) ** 2 + (points[j + 1, 1] - points[j, 1]) ** 2)\n",
    "    e = math.sqrt(b - a ** 2)\n",
    "    f = math.log((sj ** 2 + 2 * a * sj + b) / b)\n",
    "    g = math.atan((sj + a) / e) - math.atan(a / e)\n",
    "\n",
    "    return a, d, e, f, g, sj, c1, c2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8a4ae94",
   "metadata": {},
   "source": [
    "Define a function to solver for the airfoil performance at a specific angle of attack"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d62a1517",
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_panel(points, centroids, n, alpha, plot):\n",
    "    jmat = np.zeros((n, n))\n",
    "    kmat = np.zeros((n, n))\n",
    "    amat = np.zeros((n + 1, n + 1))\n",
    "    vvec = np.zeros((n + 1, 1))\n",
    "    svec = np.zeros((n + 1, 1))\n",
    "\n",
    "    # Get the coefficients for all panels on each other\n",
    "    for i in range(0, n):\n",
    "        for j in range(0, n):\n",
    "            if j != i:\n",
    "                # Get the coefficients for panel i and panel j\n",
    "                [a, d, e, f, g, sj, c1, c2] = coefficients(points, centroids, i, j)\n",
    "\n",
    "                # Build the matrices\n",
    "                jmat[i, j] = -c2 / 2 * f - c1 * g\n",
    "                kmat[i, j] = -c2 + 1 / sj * ((a * c2 + d / 2) * f + (a * c1 + e * c2) * g)\n",
    "                svec[j] = sj\n",
    "            else:\n",
    "                kmat[i, j] = -1\n",
    "\n",
    "    # Build the amat matrix, angle, and velocity\n",
    "    for i in range(0, n):\n",
    "        for j in range(0, n):\n",
    "            amat[i, j] = amat[i, j] + jmat[i, j] - kmat[i, j]\n",
    "            amat[i, j + 1] = amat[i, j + 1] + kmat[i, j]\n",
    "\n",
    "        angle = math.atan2((points[i + 1, 1] - points[i, 1]), (points[i + 1, 0] - points[i, 0]))\n",
    "        vvec[i] = 2 * math.pi * math.sin(alpha - angle)\n",
    "\n",
    "    # Add the Kutta condition constraint\n",
    "    amat[n, 0] = 1.0\n",
    "    amat[n, n] = 1.0\n",
    "\n",
    "    # Solve for the circulation\n",
    "    gam = np.linalg.solve(amat, vvec)\n",
    "\n",
    "    # Get the pressure coefficient\n",
    "    cp = 1 - (gam ** 2)\n",
    "\n",
    "    # Get the lift coefficient by integrating the circulation\n",
    "    cl = 0\n",
    "    for i in range(0, n):\n",
    "        cl = cl + 0.5 * (gam[i] + gam[i + 1]) * svec[i]\n",
    "    cl = cl[0] / 0.5\n",
    "\n",
    "    # Plot the pressure coefficient\n",
    "    plot.plot(points[2:-2, 0], -cp[2:-2, 0], label=r'$\\alpha = $' + str(alpha / math.pi * 180))\n",
    "    plot.legend()\n",
    "    plot.set_xlabel('x/c')\n",
    "    plot.set_ylabel(r'$-C_P$')\n",
    "    plot.set_title('Pressure Coefficient')\n",
    "    np.savetxt('data/cp_' + str(alpha / math.pi * 180) + '.txt', np.c_[points[2:-2, 0], cp[2:-2, 0]], delimiter=' ')\n",
    "\n",
    "    return cl"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96e023be",
   "metadata": {},
   "source": [
    "Define a function to sweep over a range of angles of attack"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74649f51",
   "metadata": {},
   "outputs": [],
   "source": [
    "def sweep(minalpha, maxalpha, alphastep, n, points, centroids, plotcl, plotcp):\n",
    "    # Initialize arrays for plot lift curve\n",
    "    alpha_plot = []\n",
    "    cl_plot = []\n",
    "\n",
    "    # Sweep over a range of angles of attack\n",
    "    alpha = minalpha\n",
    "    while alpha <= maxalpha:\n",
    "        # Solve for all panels at angle of attack alpha\n",
    "        cl = solve_panel(points, centroids, n, math.pi * alpha / 180, plotcp)\n",
    "\n",
    "        # Print the angle and lift coefficient\n",
    "        if cl < 1.3:\n",
    "            print('Angle: ', alpha, ' Cl: ', cl)\n",
    "        else:\n",
    "            print('Angle: ', alpha, ' Cl: ', cl, ' WARNING: Stall Likely!')\n",
    "\n",
    "        # Save the values\n",
    "        alpha_plot.append(alpha)\n",
    "        cl_plot.append(cl)\n",
    "\n",
    "        alpha = alpha + alphastep\n",
    "\n",
    "    # Put the point on the lift curve\n",
    "    plotcl.plot(alpha_plot, cl_plot, marker=\".\", markersize=6, label=r'$C_L$')\n",
    "    plotcl.legend()\n",
    "    plotcl.set_xlabel(r'$\\alpha$')\n",
    "    plotcl.set_ylabel(r'$C_L$')\n",
    "    plotcl.set_title('Lift Curve')\n",
    "    np.savetxt('data/cl.txt', np.c_[alpha_plot, cl_plot], delimiter=' ')\n",
    "\n",
    "    return"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6541e68c",
   "metadata": {},
   "source": [
    "Section to run the code and plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fcce14b6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialize the plots\n",
    "figgeo, plotgeo = plt.subplots()\n",
    "figcl, plotcl = plt.subplots()\n",
    "figcp, plotcp = plt.subplots()\n",
    "\n",
    "# Load the airfoil geometry points\n",
    "[n, points, centroids] = load_points(plotgeo)\n",
    "\n",
    "# Sweep over all desired angles\n",
    "minalpha = 0\n",
    "maxalpha = 10\n",
    "alphastep = 2.5\n",
    "sweep(minalpha, maxalpha, alphastep, n, points, centroids, plotcl, plotcp)\n",
    "\n",
    "# Plot the experimental lift coefficient\n",
    "data = np.loadtxt('experimental-cl.txt')\n",
    "plotcl.plot(data[:, 0], data[:, 1], label='Experiment')\n",
    "plotcl.legend()\n",
    "\n",
    "# Plot the experiment pressure coefficients and save data\n",
    "data = np.loadtxt('experimental-cp-0deg.txt')\n",
    "plotcp.plot(data[:, 0], -data[:, 1], marker=\"x\", linestyle='None', label='Experiment ' + r'$\\alpha = 0$')\n",
    "data = np.loadtxt('experimental-cp-10deg.txt')\n",
    "plotcp.plot(data[:, 0], -data[:, 1], marker=\"x\", linestyle='None', label='Experiment ' + r'$\\alpha = 10$')\n",
    "plotcp.legend()\n",
    "\n",
    "# Save all the plots and save data\n",
    "figgeo.savefig('plots/airfoil.pdf')\n",
    "figcl.savefig('plots/cl.pdf')\n",
    "figcp.savefig('plots/cp.pdf')\n",
    "\n",
    "# Show all the plots\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b86ec88",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
